using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroCam : MonoBehaviour
{
    private bool gyroEnabled;
    private Gyroscope gyro;
    private GameObject GyroControl;
    private Quaternion rot;
    private Quaternion adjustrot;

    private void Start()
    {
        GyroControl = new GameObject("Gyro Control");
        GyroControl.transform.position = transform.position;
        transform.SetParent(GyroControl.transform);
        gyroEnabled = EnableGyro();
        adjustrot = Quaternion.Euler(90f, 0f, 0f) * Quaternion.Inverse(Input.gyro.attitude);
    }


    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            Input.gyro.enabled = true;
            GyroControl.transform.rotation = Quaternion.Euler(90f, -90f, 0f);
            rot = new Quaternion(0, 0, 1, 0);
            return true;
        }
        return false;
    }

    private void Update()
    {
        if (gyroEnabled)
        {
            if (Input.touchCount == 3)
            {
                adjustrot = Quaternion.Euler(90f, 0f, 0f) * Quaternion.Inverse(Input.gyro.attitude);
            }
            transform.localRotation = adjustrot * Input.gyro.attitude * rot;
        }
    }
}
