using System.Collections;
using UnityEngine;

public class Wander : MonoBehaviour
{
    public Transform[] target;
    public bool[] flip;
    public int pos = 0;
    public float noper = 1;
    public float range = 1;
    public float duration = 5;
    public int clampRange = 30;

    bool walking = false;
    int clampCounter = 0;
    XorShiftRandom rgen;
    Animator anim;
    SpriteRenderer sRenderer;

    void Start()
    {
        anim = GetComponent<Animator>();
        rgen = new XorShiftRandom();
        sRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!walking && Clamp() && Decider())
        {
            pos++;

            if (pos >= target.Length) 
                pos = 0;

            StartCoroutine(LerpPosition(target[pos].position, duration));
        }
    }

    IEnumerator LerpPosition(Vector3 targetPosition, float duration)
    {
        float time = 0;
        Vector3 startPosition = transform.position;

        walking = true;
        anim.SetBool("Walking", walking);
        sRenderer.flipX = flip[pos];

        while (time < duration)
        {
            float t = time / duration;
            t = t * t * (3f - 2f * t);
            transform.position = Vector3.Lerp(startPosition, targetPosition, t);
            time += Time.deltaTime;
            yield return null;
        }

        transform.position = targetPosition;
        walking = false;
        anim.SetBool("Walking", walking);
    }

    bool Decider ()
    {
        float x = Mathf.Clamp(rgen.NextInt32(), -range, range);
        float y = Mathf.Clamp(rgen.NextInt32(), -range, range);
        return x > y * noper ? true : false;
    }

    bool Clamp()
    {
        if (clampCounter >= clampRange)
        {
            clampCounter = 0;
            return true;
        }

        clampCounter++;
        return false;
    }

}
